@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
<div id="code_register_1">
<div class="card">
<header class="card-header">
    <a href="{{ URL::previous() }}" class="float-right btn btn-outline-primary mt-1">Voltar</a>
    <h4 class="card-title mt-2">Informações</h4>
</header>
<article class="card-body">
<form action="{!! route('dash.configs') !!}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_method" value="PATCH">
    @csrf
    <div class="form-row">
        <div class="col form-group">
            <label>Titulo:</label>
            <input type="text" class="form-control" placeholder="" name="title" required value="{!! $data->title !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Slogan:</label>
            <input type="text" class="form-control" placeholder="" name="slogan" required value="{!! $data->slogan !!}">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
        <div class="col form-group">
            <label>Logo:</label>
            <input type="file" class="form-control" placeholder="" name="logo">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Email:</label>
            <input type="email" class="form-control" placeholder="" name="email" required value="{!! $data->email !!}">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->

    <div class="form-row">
      <div class="col form-group">
          <button type="submit" class="btn btn-warning btn-block"> Salvar  </button>
      </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->

</div> <!-- code-wrap.// -->
</aside>
@endsection
