@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
<div id="code_register_1">
<div class="card">
<header class="card-header">
	<a href="{{ URL::previous() }}" class="float-right btn btn-outline-primary mt-1">Voltar</a>
	<h4 class="card-title mt-2">Novo Produto</h4>
</header>
<article class="card-body">
	<form action="{!! route('dash.products.edit', $product->id) !!}" method="POST" enctype="multipart/form-data" autocomplete="off">
	    <input type="hidden" name="_method" value="PATCH">
			@csrf
	<div class="form-row">
		<div class="col form-group">
			<label>Nome</label>
		  	<input type="text" class="form-control" placeholder="" name="name" value="{!! $product->name !!}">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>Foto</label>
		  	<input type="file" class="form-control" name="photo" placeholder="">
		</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
<div class="form-row">
	<div class="col form-group">
		<label>Disposição do produto</label>
		<input type="text" class="form-control" placeholder="" name="location" value="{!! $product->location !!}">
	</div> <!-- form-group end.// -->
	<div class="col form-group">
			<label>Categoria:</label>
			<select class="form-control" name="id_category">
				<option value="">Selecionar</option>
				@foreach($categories as $category)
					<option value="{!! $category->id !!}" @if($product->id_category == $category->id) selected @endif>{!! $category->name !!}</option>
				@endforeach
			</select>
	</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
	<div class="form-row">
		<div class="col form-group">
				<label>Unidade:</label>
				<select class="form-control" name="unit">
					<option value="Unidade" @if($product->status == 'Unidade') activated @endif>Unidade</option>
					<option value="Pacote" @if($product->status == 'Pacote') activated @endif>Pacote</option>
					<option value="Conjunto" @if($product->status == 'Conjunto') activated @endif>Conjunto</option>
				</select>
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>SKU</label>
			<input type="text" class="form-control" name="sku" placeholder="" value="{!! $product->sku !!}">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
				<label>Situção:</label>
				<select class="form-control" name="status">
					<option value="active" @if($product->status == 'active') activated @endif>Ativo</option>
					<option value="inactive" @if($product->status == 'inactive') activated @endif>Inativo</option>
				</select>
		</div> <!-- form-group end.// -->
		</div> <!-- form-row.// -->
		<div class="form-row">
			<div class="col form-group">
				<label>Descrição:</label>
				<textarea class="form-control" name="description" rows="4" cols="80">{!! $product->description !!}</textarea>
				</div> <!-- form-group end.// -->
	</div> <!-- form-row.// -->
	<div class="form-row">
		<div class="col-2 form-group">
			<label>Cor:</label>
			<select class="form-control" name="id_color">
				<option value="">Selecionar</option>
				@foreach($colors as $color)
					<option value="{!! $color->id !!}" @if($product->id_color == $color->id) selected @endif>{!! $color->name !!}</option>
				@endforeach
			</select>
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Tamanho:</label>
				<select class="form-control" name="id_size">
					<option value="">Selecionar</option>
					@foreach($sizes as $size)
						<option value="{!! $size->id !!}" @if($product->id_size == $size->id) selected @endif>{!! $size->name !!}</option>
					@endforeach
				</select>
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Qtd. Mínima:</label>
						<input type="text" class="form-control" name="qty_min" placeholder="" value="{!! $product->qty_min !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Quantidade:</label>
						<input type="text" class="form-control" name="qty" placeholder="" value="{!! $product->qty !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Custo:</label>
						<input type="text" class="form-control" name="cost" placeholder="" value="{!! $product->cost !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Preço:</label>
						<input type="text" class="form-control" name="price" placeholder="" value="{!! $product->price !!}">
			</div> <!-- form-group end.// -->
		</div> <!-- form-row.// -->
    <div class="form-row">
      <div class="col form-group">
          <button type="submit" class="btn btn-warning btn-block"> Salvar  </button>
      </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
</form>
<form action="{!! route('dash.products.destroy', $product->id) !!}" method="POST" enctype="multipart/form-data" autocomplete="off">
	<input type="hidden" name="_method" value="DELETE">
	@csrf
	<button type="submit" class="btn btn-danger btn-block"> Excluir  </button>
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->
</div> <!-- code-wrap.// -->
	</aside>
@endsection
