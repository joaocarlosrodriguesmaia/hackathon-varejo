@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
<div id="code_register_1">
<div class="card">
<header class="card-header">
	<a href="{{ URL::previous() }}" class="float-right btn btn-outline-primary mt-1">Voltar</a>
	<h4 class="card-title mt-2">Novo Produto</h4>
</header>
<article class="card-body">
<form action="{!! route('dash.products.create') !!}" method="POST" enctype="multipart/form-data" autocomplete="off">
    @csrf
	<div class="form-row">
		<div class="col form-group">
			<label>Nome</label>
		  	<input type="text" class="form-control" name="name" placeholder="" value="{!! old('name') !!}">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>Foto</label>
		  	<input type="file" class="form-control" name="photo" placeholder="">
		</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
<div class="form-row">
	<div class="col form-group">
		<label>Disposição do produto</label>
		<input type="text" class="form-control" placeholder="" name="location" value="{!! old('location') !!}">
	</div> <!-- form-group end.// -->
	<div class="col form-group">
			<label>Categoria:</label>
			<select class="form-control" name="id_category">
				<option value="">Selecionar</option>
				@foreach($categories as $category)
					<option value="{!! $category->id !!}">{!! $category->name !!}</option>
				@endforeach
			</select>
	</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
	<div class="form-row">
		<div class="col form-group">
				<label>Unidade:</label>
				<select class="form-control" name="unit">
					<option value="Unidade">Unidade</option>
					<option value="Pacote">Pacote</option>
					<option value="Conjunto">Conjunto</option>
				</select>
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>SKU</label>
			<input type="text" class="form-control" name="sku" placeholder="" value="{!! old('sku') !!}">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
				<label>Situção:</label>
				<select class="form-control" name="status">
					<option value="active">Ativo</option>
					<option value="inactive">Inativo</option>
				</select>
		</div> <!-- form-group end.// -->
		</div> <!-- form-row.// -->
		<div class="form-row">
			<div class="col form-group">
				<label>Descrição:</label>
				<textarea name="description" class="form-control" rows="4" cols="80">{!! old('description') !!}</textarea>
			</div> <!-- form-group end.// -->
	</div> <!-- form-row.// -->
	<div class="form-row">
		<div class="col-2 form-group">
			<label>Cor:</label>
			<select class="form-control" name="id_color">
				<option value="">Selecionar</option>
				@foreach($colors as $color)
					<option value="{!! $color->id !!}">{!! $color->name !!}</option>
				@endforeach
			</select>
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Tamanho:</label>
				<select class="form-control" name="id_size">
					<option value="">Selecionar</option>
					@foreach($sizes as $size)
						<option value="{!! $size->id !!}">{!! $size->name !!}</option>
					@endforeach
				</select>
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Qtd. Mínima:</label>
						<input type="text" class="form-control" name="qty_min" placeholder="" value="{!! old('qty_min') !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Quantidade:</label>
						<input type="text" class="form-control" name="qty" placeholder="" value="{!! old('qty') !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Custo:</label>
						<input type="text" class="form-control" name="cost" placeholder="" value="{!! old('cost') !!}">
			</div> <!-- form-group end.// -->
			<div class="col-2 form-group">
				<label>Preço:</label>
						<input type="text" class="form-control" name="price" placeholder="" value="{!! old('price') !!}">
			</div> <!-- form-group end.// -->
		</div> <!-- form-row.// -->


    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Salvar  </button>
    </div> <!-- form-group// -->
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->

</div> <!-- code-wrap.// -->
	</aside>
@endsection
