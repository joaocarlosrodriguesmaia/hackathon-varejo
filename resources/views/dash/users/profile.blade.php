@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
<div id="code_register_1">
<div class="card">
<header class="card-header">
    <a href="{{ URL::previous() }}" class="float-right btn btn-outline-primary mt-1">Voltar</a>
    <h4 class="card-title mt-2">Editar</h4>
</header>
<article class="card-body">
<form>
    <div class="form-row">
        <div class="col form-group">
            <label>Nome Completo:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Email:</label>
            <input type="email" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
      <div class="col form-group">
          <label>Tipo:</label>
          <select class="form-control" name="">
            <option value="">Selecionar</option>
            <option value="Cliente">Cliente</option>
            <option value="Vendedor">Vendedor</option>
            <option value="Administrador">Administrador</option>
          </select>
      </div> <!-- form-group end.// -->
      <div class="col form-group">
          <label>Senha:</label>
          <input type="password" class="form-control" placeholder="">
      </div> <!-- form-group end.// -->
      <div class="col form-group">
          <label>Confirmar Senha:</label>
          <input type="password" class="form-control" placeholder="">
      </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Data de Nascimento:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>CPF:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
        <div class="col form-group">
            <label>Sexo:</label>
            <select class="form-control" name="">
              <option value="">Masculino</option>
              <option value="">Feminino</option>
              <option value="">Outros</option>
            </select>
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Status:</label>
            <select class="form-control" name="">
              <option value="">Ativo</option>
              <option value="">Inativo</option>
            </select>
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
        <div class="col form-group">
            <label>Telefone:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Facebook:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Instagram:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
        <div class="form-row">
          <h4 class="card-title mt-2">Endereço</h4>
          </div> <!-- form-row end.// -->
          <div class="form-row">
        <div class="col form-group">
              <label>CEP:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Rua:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Número:</label>
            <input type="text" class="form-control" placeholder="">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
      <div class="form-row">
    <div class="col form-group">
          <label>Complemento:</label>
        <input type="text" class="form-control" placeholder="">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
        <label>Cidade:</label>
        <input type="text" class="form-control" placeholder="">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
        <label>Estado:</label>
        <input type="text" class="form-control" placeholder="">
    </div> <!-- form-group end.// -->
</div> <!-- form-row end.// -->
<div class="form-row">
      <div class="col form-group">
          <button type="submit" class="btn btn-warning btn-block"> Salvar  </button>
      </div> <!-- form-group end.// -->
      <div class="col form-group">
          <button type="submit" class="btn btn-danger btn-block"> Excluir  </button>
      </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->

</div> <!-- code-wrap.// -->
    </aside>
@endsection
