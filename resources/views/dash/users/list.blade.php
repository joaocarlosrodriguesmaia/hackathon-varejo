@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
  <ul class="nav nav-pills">
    <li class="nav-item">
      <a class="nav-link" href="{!! route('dash.users.list') !!}">Todos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{!! route('dash.users.list') !!}?type=Cliente">Clientes</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{!! route('dash.users.list') !!}?type=Administrador">Administradores</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{!! route('dash.users.list') !!}?type=Vendedor">Vendedores</a>
    </li>
  </ul>
</div>
<aside class="col-sm-12">
  <table class="table table-hover shopping-cart-wrap">
  <thead class="text-muted">
  <tr>
    <th scope="col">ID</th>
    <th scope="col">Usuário</th>
    <th scope="col">E-mail</th>
    <th scope="col">Status</th>
    <th scope="col"></th>
  </tr>
  </thead>
  <tbody>
  @foreach($users as $user)
  <tr>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $user->id !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $user->name !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $user->email !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                @if($user->status == 'active')
                  <span class="btn btn-sm btn-success float-right">Ativo</span>
                @else
                  <span class="btn btn-sm btn-danger float-right">Inactive</span>
                @endif
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <td class="text-left">
        <a href="{!! route('dash.users.edit', $user->id) !!}" class="btn btn-sm btn-info btn-round">Editar</a>
      </td>
  </tr>
  @endforeach
  </tbody>
  </table>
	</aside>
@endsection
