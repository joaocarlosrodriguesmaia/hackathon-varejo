@extends('layouts.dash')
@section('content')
<script src="{!! asset('js/address.js') !!}"></script>
<aside class="col-sm-12">
<div id="code_register_1">
<div class="card">
<header class="card-header">
    <a href="" class="float-right btn btn-outline-primary mt-1">Voltar</a>
    <h4 class="card-title mt-2">Cadastro</h4>
</header>
<article class="card-body">
  <form action="{!! route('dash.users.create') !!}" method="POST" enctype="multipart/form-data" autocomplete="off">
    @csrf
    <div class="form-row">
        <div class="col form-group">
            <label>Nome Completo:</label>
            <input type="text" class="form-control" name="name" placeholder="" value="{!! old('name') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Email:</label>
            <input type="email" class="form-control" name="email" placeholder="" value="{!! old('email') !!}">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
      <div class="col form-group">
          <label>Tipo:</label>
          <select class="form-control" name="role_id">
            <option value="">Selecionar</option>
            <option value="Cliente">Cliente</option>
            <option value="Vendedor">Vendedor</option>
            <option value="Administrador">Administrador</option>
          </select>
      </div> <!-- form-group end.// -->
      <div class="col form-group">
          <label>Senha:</label>
          <input type="password" class="form-control" name="password" placeholder="">
      </div> <!-- form-group end.// -->
      <div class="col form-group">
          <label>Confirmar Senha:</label>
          <input type="password" class="form-control" name="password_confimation" placeholder="">
      </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Data de Nascimento:</label>
            <input type="text" class="form-control date" name="birthdate" placeholder="" value="{!! old('birthdate') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>CPF:</label>
            <input type="text" class="form-control cep" name="cpf" placeholder="" value="{!! old('cpf') !!}">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
        <div class="col form-group">
            <label>Sexo:</label>
            <select class="form-control" name="sex">
              <option value="">Selecionar</option>
              <option value="Masculino">Masculino</option>
              <option value="Feminino">Feminino</option>
              <option value="Outros">Outros</option>
            </select>
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Status:</label>
            <select class="form-control" name="status">
              <option value="active">Ativo</option>
              <option value="inactive">Inativo</option>
            </select>
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
    <div class="form-row">
        <div class="col form-group">
            <label>Telefone:</label>
            <input type="text" class="form-control phone" name="phone" placeholder="" value="{!! old('phone') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Facebook:</label>
            <input type="text" class="form-control" name="facebook" placeholder="" value="{!! old('facebook') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Instagram:</label>
            <input type="text" class="form-control" name="instagram" placeholder="" value="{!! old('instagram') !!}">
        </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
        <div class="form-row">
          <h4 class="card-title mt-2">Endereço</h4>
          </div> <!-- form-row end.// -->
          <div class="form-row">
        <div class="col form-group">
              <label>CEP:</label>
            <input type="text" class="form-control" name="cep" id="cep" type="text" required maxlength="9" onblur="pesquisacep(this.value);" placeholder="00000-000" value="{!! old('cep') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Rua:</label>
            <input type="text" class="form-control" id="rua" readonly name="street" placeholder="" value="{!! old('street') !!}">
        </div> <!-- form-group end.// -->
        <div class="col form-group">
            <label>Número:</label>
            <input type="text" class="form-control" name="number" placeholder="" value="{!! old('number') !!}">
        </div> <!-- form-group end.// -->
    </div> <!-- form-row end.// -->
      <div class="form-row">
    <div class="col form-group">
          <label>Complemento:</label>
        <input type="text" class="form-control" name="complement" placeholder="" value="{!! old('complement') !!}">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
        <label>Bairro:</label>
        <input type="text" class="form-control" id="bairro" readonly name="district" placeholder="" value="{!! old('district') !!}">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
        <label>Cidade:</label>
        <input type="text" class="form-control" id="cidade" readonly name="city" placeholder="" value="{!! old('city') !!}">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
        <label>Estado:</label>
        <input type="text" class="form-control" id="uf" readonly name="state" placeholder="" value="{!! old('state') !!}">
    </div> <!-- form-group end.// -->
</div> <!-- form-row end.// -->
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-block"> Salvar  </button>
    </div> <!-- form-group// -->
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->

</div> <!-- code-wrap.// -->
    </aside>
@endsection
