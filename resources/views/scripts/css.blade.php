<link href="{!! asset('plugins/fancybox/fancybox.min.css') !!}" type="text/css" rel="stylesheet">
<!-- custom style -->
<link href="{!! asset('css/uikit.css') !!}" rel="stylesheet" type="text/css"/>
<link href="{!! asset('css/responsive.css') !!}" rel="stylesheet" media="only screen and (max-width: 1200px)" />
<link href="{!! asset('css/bootstrap-custom.css') !!}" rel="stylesheet" type="text/css"/>
<!-- Font awesome 5 -->
<link href="{!! asset('fonts/fontawesome/css/fontawesome-all.min.css') !!}" type="text/css" rel="stylesheet">
