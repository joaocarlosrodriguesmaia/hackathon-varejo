<!-- jQuery -->
<script src="{!! asset('js/jquery-2.0.0.min.js') !!}" type="text/javascript"></script>
<!-- Bootstrap4 files-->
<script src="{!! asset('js/bootstrap.bundle.min.js') !!}" type="text/javascript"></script>
<!-- plugin: fancybox  -->
<script src="{!! asset('plugins/fancybox/fancybox.min.js') !!}" type="text/javascript"></script>
<!-- custom javascript -->
<script src="{!! asset('js/script.js') !!}" type="text/javascript"></script>
<script type="text/javascript">
  /// some script
  // jquery ready start
  $(document).ready(function() {
  	// jQuery code
  });
  // jquery end
</script>
