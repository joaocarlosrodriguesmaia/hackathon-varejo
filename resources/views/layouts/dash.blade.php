@php
use App\Models\Settings;
$settings = Settings::First();
@endphp
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">

<title>{!! $settings->title !!}</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">


@include('scripts.css')
@include('scripts.js')



</head>
<body>

@include('partials.nav-horizontal')
@auth
<div class="card filter-products col-md-11">
	<div class="card-body">
		<div class="row">
			<div class="col-md-21-24"> 
				<ul class="list-inline">
				  <li class="list-inline-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">   Usuarios </a>
		            <div class="dropdown-menu p-3" style="max-width:400px";>	
				      <label class="form-check">
				      	<a href="{{ route('dash.users.create') }}">Novo </a>
				      </label>
				      <label class="form-check">
				      	<a href="{{ route('dash.users.list') }}">Listar </a>
				      </label>
		            </div> <!-- dropdown-menu.// -->
			      </li>
			      <li class="list-inline-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">  Produtos </a>
		            <div class="dropdown-menu p-3" style="max-width:400px";>	
				      <label class="form-check">
				      	<a href="{{ route('dash.products.create') }}">Novo </a>
				      </label>
				      <label class="form-check">
				      	<a href="{{ route('dash.products.list') }}">Listar </a>
				      </label>
		            </div> <!-- dropdown-menu.// -->
			      </li>
			      <li class="list-inline-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">  Fornecedores </a>
		            <div class="dropdown-menu p-3" style="max-width:400px";>	
				      <label class="form-check">
				      	<a href="#">Novo </a>
				      </label>
				      <label class="form-check">
				      	<a href="#">Listar </a>
				      </label>
		            </div> <!-- dropdown-menu.// -->
			      </li>
			      <li class="list-inline-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">  Relatórios </a>
		            
			      </li>
			      <li class="list-inline-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">  Mala Direta </a>
		            
			      </li>
			</div> <!-- col.// -->
		</div> <!-- row.// -->
			</div> <!-- card-body .// -->
		</div> <!-- card.// -->
		@endauth
@include('helpers.alert')
@yield('content')

</section>
<!-- ========================= SECTION INTRO END// ========================= -->



@include('partials.footer')


</body>
</html>
