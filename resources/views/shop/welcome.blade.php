@extends('layouts.dash')
@section('content')
<!-- ========================= SECTION INTRO ========================= -->
<section class="sectional-home section-intro text-white text-center">
<div class="container d-flex flex-column"  style="min-height:70.5vh;">

<div class="row m-auto">
    <div class="col-lg-12 col-sm-12 text-center mx-auto">
        <h1 class="display-4 mb-3">Simply Easy</h1>
        <p class="lead m-auto">Uma plataforma simplesmente facil de usar</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-9 col-md-8 col-sm-12 m-auto text-center">

        <form class="form-noborder" action="{{ route('shop.search')}}" method="GET" autocomplete="off">
            <div class="form-row mb-5">

                <div class="col-lg-9 col-sm-12 form-group form-row">
                    <input id="search-input-block" name="name" class="form-control form-control-lg col-md-10" placeholder="Procure por Nome ou Código" type="text"><button class="search-input-home btn-warning col-md-2" type="submit"><i class="fas fa-search "></i></button>
                </div>
                <div class="col-lg-3 col-sm-12 form-group">
                    <a href="{!! asset('buscar-produtos') !!}" class="btn btn-warning col-sm-12">Todos</a>
                </div>
                <!-- <div class="col-lg-3 col-sm-12 form-group">
                    <a class="btn btn-danger col-sm-12" href="#">QR CODE</a>
                </div> -->
            </div>
        </form>
    </div> <!-- col.// -->
</div> <!-- row.// -->
@endsection
