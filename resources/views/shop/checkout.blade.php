@extends('layouts.dash')
@section('content')
<!-- ========================= SECTION INTRO ========================= -->
<section class="section-intro text-white text-center">
	<div class="container d-flex flex-column"  style="min-height:70.5vh;">
		<!-- ========================= SECTION CONTENT ========================= -->


<div id="checkout-page" class="row">
	<main class="col-sm-9 mt-auto">

<div class="table-responsive">
<table class="table table-hover shopping-cart-wrap">
<thead class="text-muted">
<tr>
	<th scope="col">Imagem</th>
  	<th scope="col">Produto</th>
  	<th scope="col" width="120">Quantidade</th>
  	<th scope="col" width="120">Preço</th>
  	<th scope="col" class="text-right" width="200">Ação</th>
</tr>
</thead>
<tbody>

<?php $pricetotal = 0; ?>
	@foreach($cartItems as $item)
	<tr id="tabela-checkout">
	<td>
		<?php $pricetotal = ($item->price * $item->qty) + $pricetotal; ?>
		<figure class="media">
			<div class="img-wrap"><img src="{!! asset('images/'.$item->options['photo']) !!}" class="img-thumbnail img-sm"></div>
				
		</figure> 
	</td>
	<td>
	<p class="title text-truncate">{{$item->name}} </p>
</td>
	<td>{{$item->qty}}</td>
	<td> 
		<div class="price-wrap"> 
			<var class="price"> R$ {{$item->price}}</var> 
		</div> <!-- price-wrap .// -->
	</td>
	<td class="text-right"> 
	<form action="{{ route('shop.destroy')}}" method="POST">
		@csrf		
	    <input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="id_produto" value="{{$item->rowId}}">
	   <input type="submit" class="btn btn-danger" value="Deletar"/>
	</form>
	</td>
	</tr>
	@endforeach

</tbody>
</table>
</div> <!-- card.// -->


	</main> <!-- col.// -->
	<aside id="checkout-bloco" class="col-sm-3 text-right mt-5">
	<div>
		<p>VALOR TOTAL:<span> R$ {{$pricetotal}}</span></p>
	</div>
	<div>
		<form action="{{ route('shop.finish')}}" method="post">
		<button class="btn btn-danger">Finalizar Compra</button>
		</form>
	</div>
	</aside>
</div>

<!-- ========================= SECTION CONTENT END// ========================= -->
@endsection