@extends('layouts.dash')
@section('content')
<!-- ========================= SECTION INTRO ========================= -->
<section class="section-list-product text-white text-center">
	<div class="container d-flex flex-column"  style="min-height:70.5vh;">
		
			<!-- ========================= SECTION CONTENT ========================= -->
			<div class="row-sm">
			@foreach($products as $product)
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"><img src="{!! asset('images/'.$product->photo) !!}"/></div>
						<figcaption class="info-wrap">
							<a href="{!! asset($product->id.'/produto') !!}" class="title">{{$product->name}}</a>
							<div class="price-wrap">
								<span class="price-new">${{$product->price}}</span>
								<span class="cost-new">{{$product->cost}}</span>
							</div> <!-- price-wrap.// -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
			@endforeach
</div> <!-- row.// -->
</div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->
			
@endsection