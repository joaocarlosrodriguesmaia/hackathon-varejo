@extends('layouts.dash')
@section('content')
<!-- ========================= SECTION INTRO ========================= -->
<section id="show-product" class="section-intro text-center">
<div class="container d-flex flex-column"  style="min-height:70.5vh;">

<div class="row">
<div class="col-xl-12 col-md-9 col-sm-12">
<main class="card">
	<div class="row no-gutters">
		<aside class="col-sm-6 border-right">
<article class="gallery-wrap">
<div class="img-big-wrap">
  <div> <a href="{!! asset('images/'.$product->photo) !!}" data-fancybox=""><img src="{!! asset('images/'.$product->photo) !!}"></a></div>
</div> <!-- slider-product.// -->

</article> <!-- gallery-wrap .end// -->
		</aside>
		<aside class="col-sm-6">
<article class="card-body">
<!-- short-info-wrap -->
	<h3 class="title mb-3">{{$product->name}}</h3>

<div class="mb-3">
	<var class="price h3 price-product">
		<span class="currency">Preço: R$ </span><span class="num">{{$product->price}}</span>
	</var>
</div> <!-- price-detail-wrap .// -->
<dl class="row">
  <dt class="col-sm-3">Codigo#</dt>
  <dd class="col-sm-9">{{$product->sku}}</dd>

  <dt class="col-sm-3">Localização</dt>
  <dd class="col-sm-9">{{$product->location}}</dd>
</dl>
<hr>
	<div class="row">
		<div class="col-sm-5">
			<dl class="dlist-inline">
			  <dt>Quantidade: </dt>
			  <dd>{{$product->qty}} </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		<div class="col-sm-5">
			<dl class="dlist-inline">
			  <dt>Quantidade Minima: </dt>
			  <dd>{{$product->qty_min}} </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
	</div> <!-- row.// -->
	<hr>
	@auth
	<a href="{{ route('shop.finish') }}" class="btn  btn-sell"> Finalizar Venda </a>
	<form action="{{ route('shop.add')}}" method="post">
		@csrf
		<input type="hidden" name="id" value="{{$product->id}}">
		<button type="submit" class="btn btn-outline-dark  btn-cart"><i class="fas fa-cart-plus"></i> Adicionar no Carrinho </a>
	</form>
	@endauth
<!-- short-info-wrap .// -->
</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</main> <!-- card.// -->

<!-- PRODUCT DETAIL -->
<article class="card mt-3">
	<div class="card-body">
		<h4>Detalhes do Produto</h4>
		<p>{{$product->description}}	</p>
	</div> <!-- card-body.// -->
</article> <!-- card.// -->

<!-- PRODUCT DETAIL .// -->

</div> <!-- col // -->
</div> <!-- row.// -->
</div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->
@endsection

<!--location, id_category, cor, tamanho, quantidade, qtd minima, custo, preço, status-->
