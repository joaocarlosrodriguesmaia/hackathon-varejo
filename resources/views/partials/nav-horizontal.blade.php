@php
use App\Models\Settings;
$settings = Settings::First();
@endphp
<nav  class="head-footer navbar navbar-expand-lg navbar-dark">
	<a class="navbar-brand" href="{!! route('shop.welcome') !!}"> <img class="logo-sis" src="{!! asset('images/settings/'.$settings->logo) !!}"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbar1">
    <ul class="navbar-nav ml-auto">
<li class="nav-item active">
<a class="nav-link" href="{!! route('shop.welcome') !!}">Pagina inicial <span class="sr-only">(current)</span></a>
</li>
@auth
<li class="button"><a class="btn btn-warning login-nav" href="{!! route('dash.logout') !!}" >  Sair  </a></li>
@else
<li class="button"><a class="btn btn-warning login-nav" href="{!! route('login') !!}" >  Login  </a></li>
@endauth

@auth
<li class="button"><a class="btn btn-danger cart-nav" href="{{ route('shop.finish')}}" > <i class="fa fa-cart-plus"></i> ({{Cart::count()}}) 

@endauth
</a></li>
    </ul>
  </div>
</nav>
