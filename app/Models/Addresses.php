<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
  protected $table = 'addresses';
  protected $fillable = [
     'street', 'number', 'cep',
     'district', 'state', 'city', 'complement'
  ];
}
