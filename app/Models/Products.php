<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
  protected $table = 'products';
  protected $fillable = [
     'name', 'description', 'sku', 'photo', 'location',
     'id_category', 'unit', 'id_color', 'id_size',
     'qty', 'qty_min', 'cost', 'price', 'status'
  ];

  public function color()
  {
      return $this->hasOne('App\Models\Colors');
  }

  public function size()
  {
      return $this->hasOne('App\Models\Size');
  }

  public function category()
  {
      return $this->hasOne('App\Models\Categories');
  }
}
