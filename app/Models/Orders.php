<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
  protected $table = 'orders';
  protected $fillable = [
     'client_id', 'salesman_id', 'payment_id', 'received',
     'discount', 'change', 'total', 'status'
  ];
}
