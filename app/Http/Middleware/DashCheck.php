<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DashCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::guest() )
          return redirect('/login');
        else {
          if(Auth::User()->status == 'inactive')
            return redirect()->route('dash.logout')->with('error', 'Você não pode acessar a plataforma.');
          if(Auth::User()->hasMany(['Administrador', 'Vendedor']))
            return redirect()->route('dash.logout')->with('error', 'Você não pode acessar a plataforma.');
        }
      return $next($request);
    }
}
