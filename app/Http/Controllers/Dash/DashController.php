<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class DashController extends Controller
{
    public function index()
    {
      return redirect()->route('shop.welcome');
    }

    public function logout()
    {
      Auth::logout();
      return redirect()->route('shop.welcome');
    }
}
