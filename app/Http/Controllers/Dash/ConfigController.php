<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

use App\Models\Settings;

use Validator;

class ConfigController extends Controller
{
    public function index(Request $request)
    {
      $data = Settings::first();
      if($request->isMethod('PATCH')) {
        $validator = Validator::make($request->all(),
          [
            'title'                     =>'required|max:255',
            'slogan'                    =>'required|max:255',
            'logo'                      =>'image|mimes:png,jpg,jpeg|max:10000|max:255',
            'email'                     =>'required|email|max:255'
          ],
          [
            'title.required'            => 'Informe o título.',
            'slogan.required'           => 'Informe o slogan.',
            'email.required'            => 'Informe um e-mail válido.',
            'email.email'               => 'Informe um e-mail válido.',
            'logo.image'                => 'Insira uma arquivo do tipo imagem (png, jpg ou jpeg).',
            'logo.mimes'                => 'Insira uma imagem do tipo png, jpg ou jpeg.',
            'logo.max'                  => 'Insira uma imagem menor que 10000px.'
          ]
        );
        if($validator->fails())
          return back()->withInput($request->all())->withErrors($validator->errors());
        $data->fill($request->all());
        $image = $data->logo;
        if($image) {
          if($image != 'default.png'){
            $path = public_path().'/images/settings/';
            File::delete($path.$image);
          }
          $imageName = uniqid() . $image->getClientOriginalName();
          $image->move('images/settings', $imageName);
          $data->logo = $imageName;
        }
        $data->save();
        return back()->with('success', 'Alterado com sucesso.');
      }
      $data = Settings::first();
      return view('dash.settings.index', compact('data'));
    }
}
