<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\User;
use App\Roles;
use App\Models\Addresses;

use Validator;

class UserController extends Controller
{
    public function all(Request $request)
    {
      $users = User::get();
      return view('dash.users.list', compact('users'));
    }

    public function create(Request $request)
    {
      // celular_com_ddd
      // cpf
      if($request->isMethod('post')) {
        $validator = Validator::make($request->all(),
          [
            'name'                      =>'required|max:255',
            'email'                      =>'required|max:255',
            'password'                      =>'required|max:255',
            'birthdate'                      =>'required|max:255',
            'cpf'                     =>'required|max:255',
            'phone'                      =>'required|max:255',
            'sex'                      =>'required|max:255',
            'status'                      =>'required|max:255',
           'street'                      =>'required|max:255',
           'number'                      =>'required|max:255',
           'cep'                      =>'required|max:255',
           'district'                      =>'required|max:255',
           'state'                      =>'required|max:255',
           'city'                      =>'required|max:255',
          ],
          [
            //
          ]
        );
        if($validator->fails())
          return back()->withInput($request->all())->withErrors($validator->errors());
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->birthdate = date('Y-m-d', strtotime(str_replace('/', '-', $request->birthdate)));
        $user->roles()->attach(Roles::where('name', $request->role_id)->first());
        $address = new Addresses();
        $address->fill($request->all());
        $address->save();
        $user->address_id = $address->id;
        $user->save();
        return redirect()->route('dash.users.list')->with('sucesss', 'Removido com sucesso.');
      }
      return view('dash.users.create');
    }

    public function edit(Request $request, $id)
    {
      $user = User::find($id);
      if($request->isMethod('post')) {
        $validator = Validator::make($request->all(),
          [
            'name'                      =>'required|max:255',
            'email'                      =>'required|max:255',
            'password'                      =>'required|max:255',
            'birthdate'                      =>'required|max:255',
            'cpf'                     =>'required|max:255',
            'phone'                      =>'required|max:255',
            'sex'                      =>'required|max:255',
            'status'                      =>'required|max:255',
           'street'                      =>'required|max:255',
           'number'                      =>'required|max:255',
           'cep'                      =>'required|max:255',
           'district'                      =>'required|max:255',
           'state'                      =>'required|max:255',
           'city'                      =>'required|max:255',
          ],
          [
          ]
        );
        if($validator->fails())
          return back()->withInput($request->all())->withErrors($validator->errors());
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->birthdate = date('Y-m-d', strtotime(str_replace('/', '-', $request->birthdate)));
        $user->roles()->attach(Roles::where('name', $request->role_id)->first());
        $address = Addresses::find($user->address_id);
        $address->fill($request->all());
        $address->save();
        $user->save();
      }
      return view('dash.users.edit', compact('user'));
    }

    public function destroy($id)
    {
      User::destroy($id);
      return redirect()->route('dash.products.list')->with('error', 'Removido com sucesso.');
    }
}
