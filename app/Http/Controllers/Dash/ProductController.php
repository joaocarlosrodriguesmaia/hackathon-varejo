<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Products;
use App\Models\Categories;
use App\Models\Colors;
use App\Models\Sizes;

use Validator;

class ProductController extends Controller
{
    public function all(Request $request)
    {
      $products = Products::get();
      return view('dash.products.list', compact('products'));
    }

    public function create(Request $request)
    {
      if($request->isMethod('post')) {
        $validator = Validator::make($request->all(),
          [
            'name'                      =>'required|max:255',
            'description'               =>'required|max:255',
            'sku'                       =>'required|max:255',
            'photo'                     =>'image|mimes:png,jpg,jpeg|max:10000|max:255',
            'location'                  =>'required|max:255',
            'id_category'               =>'required|max:255',
            'unit'                      =>'required|max:255',
            'id_color'                  =>'required|max:255',
            'id_size'                   =>'required|max:255',
            'qty'                       =>'required|max:255',
            'qty_min'                   =>'required|max:255',
            'cost'                      =>'required|max:255',
            'price'                     =>'required|max:255',
            'status'                    =>'required|max:255'
          ],
          [
            'name.required'             =>'Informe o nome.',
            'description.required'      =>'Informe a descrição.',
            'location.required'         =>'Informe a disposição do produto.',
            'id_category.required'      =>'Informe a categoria.',
            'unit.required'             =>'Informe a unidade.',
            'id_color.required'         =>'Informe a cor.',
            'id_size.required'          =>'Informe o tamanho.',
            'qty.required'              =>'Informe a quantidae.',
            'qty_min.required'          =>'Informe a quantidae mínima.',
            'cost.required'             =>'Informe o preço de custo.',
            'price.required'            =>'Informe o preço final.',
            'status.required'           =>'Informe o status.',
            'sku.required'              =>'Informe o SKU.',
            'photo.image'               =>'Insira uma arquivo do tipo imagem (png, jpg ou jpeg).',
            'photo.mimes'               =>'Insira uma imagem do tipo png, jpg ou jpeg.',
            'photo.max'                 =>'Insira uma imagem menor que 10000px.'
          ]
        );
        if($validator->fails())
          return back()->withInput($request->all())->withErrors($validator->errors());
        $product = new Products();
        $product->fill($request->all());
        $image = $request->photo;
        if($image) {
          $imageName = uniqid() . date('YmaHsi');
          $image->move('images/products', $imageName);
          $product->photo = $imageName;
        } else {
          $product->photo = 'default.png';
        }
        $product->save();
        return redirect()->route('dash.products.edit', $product->id)->with('success', 'Cadastro realizado com sucesso.');
      }
      $sizes = Sizes::Get();
      $colors = Colors::Get();
      $categories = Categories::Get();
      return view('dash.products.create', compact('product', 'sizes', 'colors', 'categories'));
    }

    public function edit(Request $request, $id)
    {
      $product = Products::find($id);
      if($request->isMethod('patch')) {
        $validator = Validator::make($request->all(),
          [
            'name'                      =>'required|max:255',
            'description'               =>'required|max:255',
            'sku'                       =>'required|max:255',
            'photo'                     =>'image|mimes:png,jpg,jpeg|max:10000|max:255',
            'location'                  =>'required|max:255',
            'id_category'               =>'required|max:255',
            'unit'                      =>'required|max:255',
            'id_color'                  =>'required|max:255',
            'id_size'                   =>'required|max:255',
            'qty'                       =>'required|max:255',
            'qty_min'                   =>'required|max:255',
            'cost'                      =>'required|max:255',
            'price'                     =>'required|max:255',
            'status'                    =>'required|max:255'
          ],
          [
            'name.required'             =>'Informe o nome.',
            'description.required'      =>'Informe a descrição.',
            'location.required'         =>'Informe a disposição do produto.',
            'id_category.required'      =>'Informe a categoria.',
            'unit.required'             =>'Informe a unidade.',
            'id_color.required'         =>'Informe a cor.',
            'id_size.required'          =>'Informe o tamanho.',
            'qty.required'              =>'Informe a quantidae.',
            'qty_min.required'          =>'Informe a quantidae mínima.',
            'cost.required'             =>'Informe o preço de custo.',
            'price.required'            =>'Informe o preço final.',
            'status.required'           =>'Informe o status.',
            'sku.required'              =>'Informe o SKU.',
            'photo.image'               =>'Insira uma arquivo do tipo imagem (png, jpg ou jpeg).',
            'photo.mimes'               =>'Insira uma imagem do tipo png, jpg ou jpeg.',
            'photo.max'                 =>'Insira uma imagem menor que 10000px.'
          ]
        );
        if($validator->fails())
          return back()->withInput($request->all())->withErrors($validator->errors());
        $product->fill($request->all());
        $product->price = number_format($request->price, 2, '.', '');
        $image = $request->photo;
        if($image) {
          if($image != 'default.png'){
            $path = public_path().'/images/products/';
            File::delete($path.$image);
          }
          $imageName = uniqid() . $image->getClientOriginalName();
          $image->move('images/products', $imageName);
          $product->photo = $imageName;
        }
        $product->save();
        return back()->with('success', 'Alterado com sucesso.');
      }
      $sizes = Sizes::Get();
      $colors = Colors::Get();
      $categories = Categories::Get();
      return view('dash.products.edit', compact('product', 'sizes', 'colors', 'categories'));
    }

    public function destroy($id)
    {
      Products::destroy($id);
      return redirect()->route('dash.products.list')->with('success', 'Removido com sucesso.');
    }
}
