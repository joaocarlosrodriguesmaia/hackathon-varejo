<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Gloudemans\Shoppingcart\Facades\Cart;

use App\Models\Products;

class ShopController extends Controller
{
    public function welcome(Request $request)
    {
      return view('shop.welcome');
    }

    public function search(Request $request)
    {
      $products = Products::where('name', 'LIKE', '%'.$request->name.'%')
                        ->orwhere('sku', 'LIKE', '%'.$request->name.'%')
                        ->get();
      return view('shop.list', compact('products'));
    }

    public function show($id)
    {
      $product = Products::find($id);
      return view('shop.show', compact('product'));
    }

    public function add(Request $request)
    {
      $product = Products::find($request->id);
      $attributes['photo'] = $product->photo;      
      Cart::add($request->id, $product->name, 1, $product->price, $attributes);
      return back()->with('success', 'Produto inserido.');
    }

    public function update(Request $request, $id)
    {
      $options = Cart::get($id)->options;
      $arrOptions = array();
      foreach($options as $option=>$value)
          $arrOptions[$option] = $value;
      foreach($data as $index=>$value)
          $arrOptions[$index] = $value;
      $product = Products::find(Cart::get($id)->id);
      Cart::update($id, ['qty' => $request->qty, 'options' => $arrOptions]);
      return back()->with('success', 'Produto alterado com sucesso.');
    }

    public function destroy(Request $request)
    {
      Cart::remove($request->id_produto);
      return back()->with('error', 'Produto removido com sucesso.');
    }

    public function finish(Request $request)
    {
      $cartItems = Cart::content();
      if($request->isMethod('post'))
      {
        $order = new Order();
        $order->client_id = '';
        $order->salesman_id = Auth::id();
        $order->payment_id = '';
        $order->received = $request->received;
        $order->discount = $request->discount;
        $order->change = $request->change;
        $order->total = $request->total;
        $order->status = 1;
        $order->save();
        foreach (Cart::content()->unique() as $item) {
          DB::table('order_has_products')->INSERT([
            'order_id'   => $order3->id,
            'product_id' => $item->id,
            // 'size'       => $item->id,
            'price'      => number_format($item->price, 2, '.', ''),
            'qty'        => $item->qty
          ]);
          // baixa estoque
          $product = Product::find($item->id);
          $product->qty = $product->qty - $item->qty;
          $product->save();
        }
        Cart::destroy();
        return redirect()->route('shop.welcome')->with('success', 'Venda realizada com sucesso');
      }
      return view('shop.checkout', compact('cartItems'));
    }
}
