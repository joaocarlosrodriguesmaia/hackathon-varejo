@extends('layouts.dash')
@section('content')
<aside class="col-sm-12">
  <table class="table table-hover shopping-cart-wrap">
  <thead class="text-muted">
  <tr>
    <th scope="col">SKU</th>
    <th scope="col">Nome</th>
    <th scope="col">Categoria</th>
    <th scope="col">Estoque</th>
    <th scope="col">Status</th>
    <th scope="col"></th>
  </tr>
  </thead>
  <tbody>
  @foreach($products as $product)
  <tr>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $product->sku !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $product->name !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                {!! $product->category->name !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                <strong>Cor</strong><br/>
                cor
            </div> <!-- form-group end.// -->
            <div class="col form-group">
                <strong>Tam.</strong><br/>
                tam
            </div> <!-- form-group end.// -->
            <div class="col form-group">
                <strong>Preço</strong><br/>
                {!! $product->price !!}
            </div> <!-- form-group end.// -->
            <div class="col form-group">
                <strong>Qtd.</strong><br/>
                {!! $product->qty !!}
            </div> <!-- form-group end.// -->
            <div class="col form-group">
                <strong>Qtd. Mín.</strong><br/>
                {!! $product->qty_min !!}
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <div class="form-row">
            <div class="col form-group">
                @if($product->status == 'active')
                  <span class="btn btn-sm btn-success float-right">Ativo</span>
                @else
                  <span class="btn btn-sm btn-danger float-right">Inactive</span>
                @endif
            </div> <!-- form-group end.// -->
        </div> <!-- form-row end.// -->
      </td>
      <td>
        <td class="text-left">
        <a href="{!! route('dash.products.edit', $product->id) !!}" class="btn btn-sm btn-info btn-round">Editar</a>
      </td>
  </tr>
  @endforeach
  </tbody>
  </table>
	</aside>
@endsection
