<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersHasRole
 */
class UsersHasRole extends Model
{
    protected $table = 'roles_user';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'roles_id'
    ];

    protected $guarded = [];

}
