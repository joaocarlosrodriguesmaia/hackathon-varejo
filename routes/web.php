<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/', 'as' => 'shop.'], function () {
  Route::match( array( 'GET' ), '/', array('as' => 'welcome', 'uses' => 'ShopController@welcome'));
  Route::match( array( 'GET' ), '/buscar-produtos', array('as' => 'search', 'uses' => 'ShopController@search'));
  Route::match( array( 'GET' ), '/{ID}/produto', array('as' => 'show', 'uses' => 'ShopController@show'));
  Route::match( array( 'POST' ), '/adicionar', array('as' => 'add', 'uses' => 'ShopController@add'));
  Route::match( array( 'PUT' ), '/alterar', array('as' => 'update', 'uses' => 'ShopController@update'));
  Route::match( array( 'DELETE' ), '/excluir', array('as' => 'destroy', 'uses' => 'ShopController@destroy'));
  Route::match( array( 'GET', 'POST' ), '/finalizar', array('as' => 'finish', 'uses' => 'ShopController@finish'))->middleware('auth');
});

Auth::routes();

Route::group(['prefix' => 'painel'/*, 'middleware' => 'dash'*/, 'as' => 'dash.'], function () {
  Route::match( array( 'GET' ), '/', array('as' => 'index', 'uses' => 'Dash\DashController@index'));
  Route::match( array( 'GET' ), '/sair', array('as' => 'logout', 'uses' => 'Dash\DashController@logout'));
  Route::match( array( 'GET' ), '/minha-conta', array('as' => 'myaccount', 'uses' => 'Dash\UserController@myaccount'));
  Route::match( array( 'GET', 'PATCH' ), '/configuracoes', array('as' => 'configs', 'uses' => 'Dash\ConfigController@index'));
  Route::group(['prefix' => '/usuarios', 'as' => 'users.'], function () {
    Route::match( array( 'GET', ), '/', array('as' => 'list', 'uses' => 'Dash\UserController@all'));
    Route::match( array( 'GET', 'POST' ), '/novo', array('as' => 'create', 'uses' => 'Dash\UserController@create'));
    Route::match( array( 'GET', 'PATCH' ), '/{ID}', array('as' => 'edit', 'uses' => 'Dash\UserController@edit'));
    Route::match( array( 'DELETE' ), '/{ID}', array('as' => 'destroy', 'uses' => 'Dash\UserController@destroy'));
  });
  Route::group(['prefix' => '/produtos', 'as' => 'products.'], function () {
    Route::match( array( 'GET', ), '/todos', array('as' => 'list', 'uses' => 'Dash\ProductController@all'));
    Route::match( array( 'GET', 'POST' ), '/novo', array('as' => 'create', 'uses' => 'Dash\ProductController@create'));
    Route::match( array( 'GET', 'PATCH' ), '/{ID}', array('as' => 'edit', 'uses' => 'Dash\ProductController@edit'));
    Route::match( array( 'DELETE' ), '/{ID}', array('as' => 'destroy', 'uses' => 'Dash\ProductController@destroy'));
  });
  Route::group(['prefix' => '/estoque', 'as' => 'stock.'], function () {
    Route::match( array( 'GET' ), '/', array('as' => 'list', 'uses' => 'Dash\StockController@all'));
  });
  Route::group(['prefix' => '/pedidos', 'as' => 'orders.'], function () {
    Route::match( array( 'GET', ), '/', array('as' => 'list', 'uses' => 'Dash\OrderController@all'));
    Route::match( array( 'GET', 'PATCH' ), '/{ID}', array('as' => 'edit', 'uses' => 'Dash\OrderController@show'));
    Route::match( array( 'GET', 'PATCH' ), '/{ID}/editar', array('as' => 'edit', 'uses' => 'Dash\OrderController@edit'));
  });
  Route::group(['prefix' => '/mala-direta', 'as' => 'directmail.'], function () {
    Route::match( array( 'GET', 'POST' ), '/', array('as' => 'list', 'uses' => 'Dash\DirectEmailController@all'));
    Route::match( array( 'GET', 'PATCH' ), '/{ID}', array('as' => 'edit', 'uses' => 'Dash\DirectEmailController@edit'));
    Route::match( array( 'GET', '' ), '/{ID}/enviar', array('as' => 'send', 'uses' => 'Dash\DirectEmailController@send'));
    Route::match( array( 'DELETE' ), '/{ID}', array('as' => 'destroy', 'uses' => 'Dash\DirectEmailController@destroy'));
  });
});
